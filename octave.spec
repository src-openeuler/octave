%global octave_api api-v53
%global macrosdir %(d=%{_rpmconfigdir}/macros.d; [ -d $d ] || d=%{_sysconfdir}/rpm; echo $d)
%global builddocs 1
%bcond_without qt5
Name:                octave
Epoch:               6
Version:             5.1.0
Release:             3
Summary:             A high-level language for numerical computations
License:             GPLv3+
URL:                 http://www.octave.org
Source0:             https://ftp.gnu.org/gnu/octave/octave-%{version}.tar.lz
Source2:             xorg.conf
Source3:             octave-5.1.0-docs.tar.lz
Patch0:              octave-clear-the-eof-condition.patch
Patch1:              fix-build-error-streampos-is-member-of-std.patch
Provides:            octave(api) = %{octave_api}
Provides:            bundled(gnulib)
Provides:            bundled(qterminal)
Provides:            bundled(amos)
Provides:            bundled(blas-xtra)
Provides:            bundled(daspk)
Provides:            bundled(dasrt)
Provides:            bundled(dassl)
Provides:            bundled(faddeeva)
Provides:            bundled(lapack-xtra)
Provides:            bundled(odepack)
Provides:            bundled(ordered-qz)
Provides:            bundled(quadpack)
Provides:            bundled(ranlib)
Provides:            bundled(slatec-err)
Provides:            bundled(slatec-fn)
BuildRequires:       lzip
BuildRequires:       automake libtool desktop-file-utils libappstream-glib
BuildRequires:       arpack-devel openblas-devel bison bzip2-devel curl-devel fftw-devel flex
BuildRequires:       fltk-devel ftgl-devel gcc-c++ gcc-gfortran ghostscript gl2ps-devel glpk-devel
BuildRequires:       gperf GraphicsMagick-c++-devel hdf5-devel java-devel less
BuildRequires:       libsndfile-devel libX11-devel llvm-devel mesa-libGL-devel mesa-libGLU-devel
BuildRequires:       mesa-libOSMesa-devel ncurses-devel pcre-devel portaudio-devel qhull-devel
BuildRequires:       qrupdate-devel qscintilla-qt5-devel qt5-linguist qt5-qttools-devel
BuildRequires:       readline-devel suitesparse-devel sundials-devel tex(dvips) texinfo texinfo-tex
BuildRequires:       texlive-collection-fontsrecommended texlive-ec texlive-metapost zlib-devel
BuildRequires:       mesa-dri-drivers xorg-x11-apps
BuildRequires:       zip
BuildRequires:       libXcursor-devel libXfixes-devel libXext-devel libXinerama-devel
Requires:            epstool hdf5 hicolor-icon-theme java-headless less info
Requires:            texinfo tex(amssymb.sty) tex(fontenc.sty) tex(graphicx.sty) tex(hyperref.sty)
Requires:            tex(inputenc.sty) tex(listings.sty) tex(lmodern.sty) tex(mathtools.sty)
Requires:            tex(titlesec.sty) tex(xcolor.sty) tex(color.sty) tex(epsfig.sty)
Requires:            tex(geometry.sty) tex(graphicx.sty)
%description
GNU Octave is a high-level language, primarily intended for numerical
computations. It provides a convenient command line interface for
solving linear and nonlinear problems numerically, and for performing
other numerical experiments using a language that is mostly compatible
with Matlab. It may also be used as a batch-oriented language. Octave
has extensive tools for solving common numerical linear algebra
problems, finding the roots of nonlinear equations, integrating
ordinary functions, manipulating polynomials, and integrating ordinary
differential and differential-algebraic equations. It is easily
extensible and customizable via user-defined functions written in
Octave's own language, or using dynamically loaded modules written in
C++, C, Fortran, or other languages.

%package devel
Summary:             Development headers and files for Octave
Requires:            %{name} = %{epoch}:%{version}-%{release} gcc-c++ gcc-gfortran
Requires:            fftw-devel hdf5-devel openblas-devel
Requires:            readline-devel zlib-devel libappstream-glib
%description devel
The octave-devel package contains files needed for developing
applications which use GNU Octave.

%package doc
Summary:             Documentation for Octave
BuildArch:           noarch
%description doc
This package contains documentation for Octave.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%global enable64 no
export F77=gfortran
%if !%{builddocs}
%global disabledocs --disable-docs
%endif
libjvm=$(find /usr/lib/jvm/jre/lib -name libjvm.so -printf %h)
export JAVA_HOME=%{java_home}
export CPPFLAGS=-I%{_includedir}/suitesparse
export CXXFLAGS="$(echo %optflags | sed s/-Wp,-D_GLIBCXX_ASSERTIONS//)"
%configure --enable-shared --disable-static --enable-64=%enable64 \
 --enable-float-truncate \
 %{?disabledocs} \
 --disable-silent-rules \
 --with-java-includedir=/usr/lib/jvm/java/include \
 --with-java-libdir=$libjvm \
 --with-qrupdate \
 --with-amd --with-umfpack --with-colamd --with-ccolamd --with-cholmod \
 --with-cxsparse \
 --disable-jit
make liboctave/version.h
if ! grep -q '^#define OCTAVE_API_VERSION "%{octave_api}"' liboctave/version.h
then
  echo "octave_api variable in spec does not match liboctave/version.h"
  exit 1
fi
%make_build

%install
%make_install
make install-data install-html install-info install-pdf DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_pkgdocdir}
cp -ar AUTHORS BUGS ChangeLog examples NEWS README %{buildroot}%{_pkgdocdir}/
cp -a doc/refcard/*.pdf %{buildroot}%{_pkgdocdir}/
rm -f %{buildroot}%{_infodir}/dir
tar xvf %SOURCE3 -C %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/octave/%{version}" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/octave-%{_arch}.conf
perl -pi -e "s,%{buildroot},," %{buildroot}%{_libdir}/%{name}/ls-R
perl -pi -e "s,%{buildroot},," %{buildroot}%{_datadir}/%{name}/ls-R
touch %{buildroot}%{_datadir}/%{name}/ls-R
desktop-file-validate %{buildroot}%{_datadir}/applications/org.octave.Octave.desktop
sed -i -e /project_group/d %{buildroot}/%{_datadir}/metainfo/org.octave.Octave.appdata.xml
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/metainfo/org.octave.Octave.appdata.xml
HOST_TYPE=`%{buildroot}%{_bindir}/octave-config -p CANONICAL_HOST_TYPE`
mkdir -p %{buildroot}%{_libdir}/%{name}/site/oct/%{octave_api}/$HOST_TYPE
mkdir -p %{buildroot}%{_libdir}/%{name}/site/oct/$HOST_TYPE
mkdir -p %{buildroot}%{_datadir}/%{name}/packages
mkdir -p %{buildroot}%{_libdir}/%{name}/packages
touch %{buildroot}%{_datadir}/%{name}/octave_packages
for include in octave-config defaults
do
   mv %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}.h \
      %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}-%{__isa_bits}.h
   cat > %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}.h <<EOF
EOF
done
for script in octave-config-%{version} mkoctfile-%{version}
do
   mv %{buildroot}%{_bindir}/${script} %{buildroot}%{_libdir}/%{name}/%{version}/${script}
   cat > %{buildroot}%{_bindir}/${script} <<EOF
ARCH=\$(uname -m)
case \$ARCH in
x86_64 | ia64 | s390x | aarch64 | ppc64 | ppc64le) LIB_DIR=/usr/lib64
                       SECONDARY_LIB_DIR=/usr/lib
                       ;;
* )
                       LIB_DIR=/usr/lib
                       SECONDARY_LIB_DIR=/usr/lib64
                       ;;
esac
if [ ! -x \$LIB_DIR/%{name}/%{version}/${script} ] ; then
  if [ ! -x \$SECONDARY_LIB_DIR/%{name}/%{version}/${script} ] ; then
    echo "Error: \$LIB_DIR/%{name}/%{version}/${script} not found"
    if [ -d \$SECONDARY_LIB_DIR ] ; then
      echo "   and \$SECONDARY_LIB_DIR/%{name}/%{version}/${script} not found"
    fi
    exit 1
  fi
  LIB_DIR=\$SECONDARY_LIB_DIR
fi
exec \$LIB_DIR/%{name}/%{version}/${script} "\$@"
EOF
   chmod +x %{buildroot}%{_bindir}/${script}
done
%if %{builddocs}
sed -i -e '/^# Created by Octave/d' %{buildroot}%{_datadir}/%{name}/%{version}/etc/doc-cache
%else
cp -p doc/interpreter/macros.texi %{buildroot}%{_datadir}/%{name}/%{version}/etc/macros.texi
%endif
#mkdir -p %{buildroot}%{macrosdir}
#cp -p %SOURCE1 %{buildroot}%{macrosdir}

%check
cp %SOURCE2 .
if [ -x /usr/libexec/Xorg ]; then
   Xorg=/usr/libexec/Xorg
else
   Xorg=/usr/libexec/Xorg.bin
fi
$Xorg -noreset +extension GLX +extension RANDR +extension RENDER -logfile ./xorg.log -config ./xorg.conf :99 &
sleep 2
export DISPLAY=:99
make check
%ldconfig_scriptlets

%files
%license COPYING
%{_pkgdocdir}/AUTHORS
%{_pkgdocdir}/BUGS
%{_pkgdocdir}/ChangeLog
%{_pkgdocdir}/NEWS
%{_pkgdocdir}/README
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/octave-*.conf
%{_bindir}/octave*
%{_libdir}/octave/
%{_libexecdir}/octave/
%{_mandir}/man1/octave*.1.*
%{_infodir}/liboctave.info*
%{_infodir}/octave.info*
%{_datadir}/applications/org.octave.Octave.desktop
%{_datadir}/icons/hicolor/*/apps/octave.png
%{_datadir}/icons/hicolor/scalable/apps/octave.svg
%{_datadir}/metainfo/org.octave.Octave.appdata.xml
%dir %{_datadir}/octave
%{_datadir}/octave/%{version}/
%{_datadir}/octave/ls-R
%ghost %{_datadir}/octave/octave_packages
%{_datadir}/octave/packages/
%{_datadir}/octave/site/

%files devel
%{_bindir}/mkoctfile
%{_bindir}/mkoctfile-%{version}
%{_includedir}/octave-%{version}/
%{_libdir}/pkgconfig/octave.pc
%{_libdir}/pkgconfig/octinterp.pc
%{_mandir}/man1/mkoctfile.1.*

%files doc
%{_pkgdocdir}-%{version}/examples/
%{_pkgdocdir}-%{version}/liboctave.html/
%{_pkgdocdir}-%{version}/liboctave.pdf
%{_pkgdocdir}-%{version}/octave.html
%{_pkgdocdir}-%{version}/octave.pdf
%{_pkgdocdir}-%{version}/refcard*.pdf
%{_pkgdocdir}/examples/
%{_pkgdocdir}/liboctave.html/
%{_pkgdocdir}/liboctave.pdf
%{_pkgdocdir}/octave.html
%{_pkgdocdir}/octave.pdf
%{_pkgdocdir}/refcard*.pdf

%changelog
* Wed Sep 11 2024 wangkai <13474090681@163.com> - 6:5.1.0-3
- Delete requires gnuplot because the package license is not allow

* Fri Aug 16 2024 zhangxianting <zhangxianting@uniontech.com> - 6:5.1.0-2
- fix build error, streampos is a member of 'std', not 'std::ios'

* Fri Jul 16 2021 huanghaitao <huanghaitao8@huawei.com> - 5.1.0-1
- package init
